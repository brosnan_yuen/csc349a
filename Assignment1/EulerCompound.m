function q = EulerCompound(r, P0, t0, tn, n )
%  print headings and initial conditions
fprintf('values of t    approximations v(t)\n')
fprintf('%8.3f',t0),fprintf('%19.4f\n',P0)
%  compute step size h
h=(tn-t0)/n;
%  set t,p to the initial values
t=t0;
p=P0;
%  compute v(t) over n time steps using Euler's method
q = [t0 P0];
for i=1:n
p=p+(r*p)*h;
t=t+h;
fprintf('%8.3f',t),fprintf('%19.4f\n',p)
q(end+1,:) = [t p];
end

end
