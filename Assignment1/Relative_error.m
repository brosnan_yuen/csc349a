function y = Relative_error(a , b)
if (a == b)
y = 0;
else
y = abs((a/b)-1);
end
end