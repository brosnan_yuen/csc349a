function v = fallingvelocity(g , m,c,t)
v = ((g*m)/c)*(1-exp(-( (c*t) /m)));
end