function x = forwardsub( a,b )

x = [];
x(1) = (b(1)/a(1,1));

for i = 2:length(b)

    sum = b(i);

    for j = 1:(i-1)

        sum  = sum - a(i,j)*x(j);

    end

    x(i)  = (sum/a(i,i));

end 

end

