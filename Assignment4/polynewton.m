function [root,b] = polynewton(n, a, x0, imax, eps)

i=1;
fprintf ( ' iteration approximation \n');
while (i <= imax)
    
    [b, c] = horner( x0, n, a );
    root = x0 - b(1)/c(2);
    
    fprintf ( ' %6.0f %18.8f \n', i, root);
    if((abs( 1-(x0/root) )) < eps)
        return;
    end
    i=i+1;
    x0 = root;
end
fprintf ( ' failed to converge in %g iterations\n', imax );

end